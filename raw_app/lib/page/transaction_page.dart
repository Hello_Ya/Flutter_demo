
import 'package:flutter/material.dart';
import 'package:raw_app/Model/BitInfoBean.dart';
import 'package:raw_app/ext/constant.dart';
import 'package:raw_app/page/about_page.dart';


class TransactionPage  extends StatefulWidget{


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new TransactionPageState();
  }


}

class TransactionPageState extends State<TransactionPage>{

  final List<BitInfoBean> bitListData = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initData();
  }

  void initData() async {

    setState(() {

              for (int i=0; i<30;i++) {
                if (i%2 == 0) {

                  bitListData.add(new BitInfoBean('assets/btc.png', 'BTC', '156458412',
                      '23154', '484468', '0.2214 conis', '0.245%', false));

                } else {
                  bitListData.add(new BitInfoBean('assets/eth.png', 'ETH', '9764564',
                      '88654', '33255', '0.154 conis', '0.335%', true));
                }

              }

    });


  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
     return new Scaffold(
       appBar: new AppBar(
         title: const Text("Transaction"),
         centerTitle: true,
         backgroundColor: Colors.black54,
         leading: new Padding(
           padding: const EdgeInsets.all(2.0),
           child: new IconButton(
               icon: new SizedBox(width: 20.0, height: 20.0, child: constant.nav_scan),
               onPressed: () {
              
                 Navigator.push(context, new MaterialPageRoute(
                   //方法1  builder: (context) => AboutPage()
                   builder: (BuildContext context){

                     return new AboutPage();
                   }
                 ),
                 );

                 print("扫描二维码=========");
               }),
         ),
         actions: <Widget>[
           new Padding(
             padding: const EdgeInsets.all(2.0),
             child: new IconButton(
                 icon:
                 new SizedBox(width: 20.0, height: 20.0, child: constant.nav_QRcode),
                 onPressed: () {


                   print('二维码展示');
                 }),
           )
         ],

       ),

        body:


            new Container(
              color: const Color(0xff2C3858),
              width: double.infinity,
              child: new ListView.builder(

                shrinkWrap: false,
                itemCount: bitListData.length,
                itemBuilder: (content,index){
                  return new Card(
                    margin: const EdgeInsets.all(8.0),

                    color: const Color(0x004E5B7C),
                    child: new Container(
                      padding: const EdgeInsets.all(4.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new Image.asset(bitListData[index].image_url,width: 30.0,height: 30.0,alignment: Alignment.centerLeft,),
                          new  Column(

                            children: <Widget>[
                              new Text(bitListData[index].token_name,style: TextStyle(color: Colors.white)),
                              new Text(bitListData[index].token_money,style: TextStyle(color: Colors.white))

                            ],

                          ),

                          new Column(
                            children: <Widget>[
                              new Text(bitListData[index].token_value,style: TextStyle(color: Colors.white)),
                              new Text(bitListData[index].token_coins,style: TextStyle(color: Colors.white))

                            ],

                          ),

                          new Column(

                            children: <Widget>[

                              new Text(bitListData[index].token_subvalue,style: TextStyle(color: Colors.green)),
                              new Row(
                                children: <Widget>[
                                  new Text(bitListData[index].token_isup ? "↑" : "↓",style: TextStyle(color: Colors.green),),
                                  new Text(bitListData[index].token_wave,style: TextStyle(color: Colors.green))

                                ],

                              )
                            ],

                          )


                        ],

                      ),

                    ),


                  );

                },


              ),

            ),


     );

  }

}