import 'package:flutter/material.dart';
import 'package:css_colors/css_colors.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:math';
import 'package:raw_app/ext/constant.dart';
class MePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return new MePageState();
  }
}

class MePageState extends State<MePage> {
  final items = new List<String>.generate(10000, (i) => "Item $i");

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final blueGradient = const LinearGradient(colors: const <Color>[
      const Color(0xFF0075D1),
      const Color(0xFF00A2E3),
    ], stops: const <double>[
      0.4,
      0.6
    ], begin: Alignment.topRight, end: Alignment.bottomLeft);
    final purpleGraient = const LinearGradient(
        colors: const <Color>[const Color(0xFF882DEB), const Color(0xFF9A4DFF)],
        stops: const <double>[0.5, 0.7],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight);
    final redGradient = const LinearGradient(colors: const <Color>[
      const Color(0xFFBA110E),
      const Color(0xFFCF3110),
    ], stops: const <double>[
      0.6,
      0.8
    ], begin: Alignment.bottomRight, end: Alignment.topLeft);

    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: const Color(0xff2C3858),
          elevation: 0.0,
          title: const Text('我的'),
        ),
        body: new Scaffold(
            body: new Container(
          color: const Color(0xff2C3858),
          child: new Column(
            children: <Widget>[
              new Container(
                constraints: const BoxConstraints(maxHeight: 240.0),
                margin: const EdgeInsets.only(top: 20.0),
                child: new Align(
                  alignment: Alignment.center,
                  child: new ListView(
                      shrinkWrap: true,
                      padding: const EdgeInsets.only(
                          left: 10.0, bottom: 20.0, right: 10.0, top: 10.0),
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        _buildAction(
                            "BOBO's Wallet1",
                            () {},
                            Colors.blue,
                            blueGradient,
                            new AssetImage("assets/images/microphone.png")),
                        _buildAction(
                            "BOBO's Wallet2",
                            () {},
                            Colors.purple,
                            purpleGraient,
                            new AssetImage("assets/images/wallet.png")),
                        _buildAction(
                            "BOBO's Wallet3",
                            () {},
                            Colors.red,
                            redGradient,
                            new AssetImage("assets/images/joystick.png")),
                      ]),
                ),
              ),
              _buildItem("BOBO's Wallet1", () {},
                   "assets/btc.png"),
              _buildItem("BOBO's Wallet1", () {},
                 "assets/btc.png"),
              _buildItem("BOBO's Wallet1", () {},
                  "assets/btc.png"),
              _buildItem("BOBO's Wallet1", () {},
                  "assets/btc.png"),
              _buildItem("BOBO's Wallet1", () {},
                  "assets/btc.png"),
            ],
          ),
        ))

        ///Gridview
        /*
      new GridView.count(
        crossAxisCount: 3,
        children: new List.generate(100, (index){
          return new GestureDetector(
            onTap: (){
              _Toast("Item $index");
            },
            child: new Center(
              child: new Text("Item $index", style: Theme.of(context).textTheme.headline,),

            ),
          );
        }),
      )
      
      */

        ///listview

        /*   new ListView.builder(
            itemCount: items.length,
          itemBuilder: (content,index){
            return new ListTile(
              title: new Text('${items[index]}'),
              onTap: (){
                setState(() {

                  _Toast("${items[index]}");
                });

              },
            );

          }),
*/

        ///滑动删除

        /*     new ListView.builder(
          itemCount: items.length,
          itemBuilder: (context,index){
            var item = items[index];

            return new Dismissible(
                key: new Key(item),
                onDismissed: (direction){
                  items.removeAt(index);
                  Scaffold.of(context).showSnackBar(
                      new SnackBar(content: new Text("$item dismissed")));

                },
                background: new Container(color: Colors.red,
                  child: new Text("左滑删除",style: TextStyle(color: Colors.white),),),
                child:new ListTile(title: new Text('$item'),) );
          }
          )

*/
        );
  }

  //卡片列表
  Widget _buildAction(String title, VoidCallback action, Color color,
      Gradient gradient, ImageProvider backgroundImage) {
    final textStyle = new TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontSize: 18.0,
    );

    return new GestureDetector(
      onTap: action,
      child: new Container(
        margin: const EdgeInsets.only(right: 5.0, left: 5.0),
        width: 320.0,
        decoration: new BoxDecoration(
            color: color,
            shape: BoxShape.rectangle,
            borderRadius: new BorderRadius.circular(10.0),
            boxShadow: <BoxShadow>[
              new BoxShadow(
                  color: Colors.black38,
                  blurRadius: 2.0,
                  spreadRadius: 1.0,
                  offset: new Offset(0.0, 1.0)),
            ],
            gradient: gradient),
        child: new Stack(
          children: <Widget>[
            new Opacity(
              opacity: 0.2,
              child: new Align(
                alignment: Alignment.centerRight,
                child: new Transform.rotate(
                  angle: -PI / 4.8,
                  alignment: Alignment.centerRight,
                  child: new ClipPath(
                    clipper: new _BackgroundImageClipper(),
                    child: new Container(
                      padding: const EdgeInsets.only(
                          bottom: 20.0, right: 0.0, left: 60.0),
                      child: new Image(
                        width: 90.0,
                        height: 90.0,
                        image: backgroundImage != null
                            ? backgroundImage
                            : new AssetImage("assets/images/microphone.png"),
                      ),
                    ),
                  ),
                ),
              ),
            ), // END BACKGROUND IMAGE

            new Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.only(left: 10.0, top: 10.0),
              child: new Text(title, style: textStyle),
            ),
          ],
        ),
      ),
    );
  }

  void _Toast(String s) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(s)));
  }



  ///菜单
  _buildItem(String title, VoidCallback action, String iconpic) {
    final textStyle = new TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.w700,
      fontSize: 18.0,
    );

    return new InkWell(
      onTap: action,
      child: new Padding(
        padding: const EdgeInsets.only(
            left: 10.0, right: 10.0, bottom: 5.0, top: 5.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
                width: 35.0,
                height: 35.0,
                margin: const EdgeInsets.only(right: 10.0),
                decoration: new BoxDecoration(
                  color: Colors.purple,
                  borderRadius: new BorderRadius.circular(5.0),
                ),
                alignment: Alignment.center,
                child: new Image.asset(iconpic,  width: 30.0,height: 30.0,)
            ),

            new Text(title, style: textStyle),
            new Expanded(child: new Container()),

          ],
        ),
      ),

    );
  }



}

class _BackgroundImageClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = new Path();
    path.moveTo(0.0, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
