import 'package:flutter/material.dart';
import 'dart:async';
import 'package:raw_app/Model/BitInfoBean.dart';
import 'package:raw_app/ext/constant.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';

class WalletPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return new WalletPageState();
  }
}

class WalletPageState extends State<WalletPage> {
  final List<BitInfoBean> bitListData = new List();

  /// listview  方式3

  Widget buildListBitInfo(BuildContext context, String item) {
    return new ListTile(
      isThreeLine: true,
      dense: false,
      leading: new CircleAvatar(
        child: new Text(item),
      ),
      title: new Text("子item的标题"),
      subtitle: new Text('子item的内容'),
      trailing: new Icon(
        Icons.close,
        color: Colors.red,
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initData();
  }

  void initData() async {
    setState(() {
      for (int i = 0; i < 30; i++) {
        bitListData.add(new BitInfoBean('assets/btc.png', 'BTC', '156458412',
            '23154', '484468', '0.2214 conis', '0.245%', false));
        bitListData.add(new BitInfoBean('assets/eth.png', 'ETH', '156458412',
            '23154', '484468', '0.2214 conis', '0.245%', true));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    ///listview   方式2
    List<Widget> listbit = new List();

    for (int i = 0; i < bitListData.length; i++) {
      if (i == 0) {
        listbit.add(
          new Column(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new Text("Total Balance:",
                      style: TextStyle(color: Colors.white70, fontSize: 20.0),
                      textAlign: TextAlign.start),
                  new Text(
                    '==¥ 6516125.00',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 36.0,
                        fontWeight: FontWeight.bold),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "+ ¥170.38",
                        style:
                            TextStyle(color: Color(0xff00ff00), fontSize: 20.0),
                      ),
                      new Text('20.34%' + '  ↑',
                          style: TextStyle(
                              color: Color(0xff00ff00), fontSize: 20.0)),
                      new Text("(Today)",
                          style: TextStyle(
                              color: Color(0xff00ff00), fontSize: 20.0))
                    ],
                  ),
                ],
              ),
            ],
          ),
        );
      } else {
        listbit.add(new Card(
          margin: const EdgeInsets.all(8.0),
          color: const Color(0x004E5B7C),
          child: new Container(
            padding: const EdgeInsets.all(8.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Image.asset(
                  bitListData[i - 1].image_url,
                  width: 40.0,
                  height: 40.0,
                  alignment: Alignment.centerLeft,
                ),
                new Column(
                  children: <Widget>[
                    new Text(
                      bitListData[i - 1].token_name,
                      style: _textFun,
                    ),
                    new Text(bitListData[i - 1].token_money, style: _textFun)
                  ],
                ),
                new Column(
                  children: <Widget>[
                    new Text(
                      bitListData[i - 1].token_value,
                      style: _textFun1,
                    ),
                    new Text(
                      bitListData[i - 1].token_coins,
                      style: _textFun1,
                    )
                  ],
                ),
                new Column(
                  children: <Widget>[
                    new Text(
                      bitListData[i - 1].token_subvalue,
                      style: _textFun2,
                    ),
                    new Row(
                      children: <Widget>[
                        new Text(
                          bitListData[i - 1].token_isup ? "↑" : "↓",
                          style: TextStyle(
                              color: Color(0x0000ff00), fontSize: 18.0),
                        ),
                        new Text(bitListData[i - 1].token_wave,
                            style: _textFun2)
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ));
      }
    }

    ///listview 方式 3

    List<String> datas = <String>['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J'];
    Iterable<Widget> listdatas = datas.map((String item) {
      return buildListBitInfo(context, item);
    });
    listdatas = ListTile.divideTiles(context: context, tiles: listdatas);

    return new Scaffold(
      appBar: new AppBar(
        title: new GestureDetector(
          onTap: _showPopWindow(),
          child:  Text("BOB's Wallet"),
        ),
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: const Color(0xff2C3858),
        leading: new Padding(
          padding: const EdgeInsets.all(2.0),
          child: new IconButton(
              icon: new SizedBox(
                  width: 20.0, height: 20.0, child: constant.nav_scan),
              onPressed: () {
                //_scanQr();

                print("扫描二维码=========");
              }),
        ),
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(2.0),
            child: new IconButton(
                icon: new SizedBox(
                    width: 20.0, height: 20.0, child: constant.nav_QRcode),
                onPressed: () {
                  _showSnackBar('二维码======');

                  print('二维码展示');
                }),
          )
        ],
      ),
      body: new Container(
          color: const Color(0xff2C3858),
          width: double.infinity,
          child: new ListView(
            physics: new AlwaysScrollableScrollPhysics(),
            children: listbit,
          )),
    );
  }

  void _showSnackBar(message) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(message)));
  }

  TextStyle _textFun = new TextStyle(
    color: Colors.white,
    fontSize: 20.0,
  );
  TextStyle _textFun1 = new TextStyle(
    color: Colors.white,
    fontSize: 16.0,
  );
  TextStyle _textFun2 = new TextStyle(
    color: Colors.green,
    fontSize: 18.0,
  );

   _showPopWindow() {
        print('----------------------------------');
    setState(() {

      new PopupMenuButton<String>(
        itemBuilder: (BuildContext content) {
          return bitListData.skip(2).map((BitInfoBean bitinfo) {
            return new PopupMenuItem(
              child: new Text(bitinfo.token_name),
              value: bitinfo.token_value,
            );
          }).toList();
        },
        onSelected: null,
      );

    });


  }

  String _result = "Lets start to scan";

  Future _scanQr() async {
    debugPrint("scan touched");
    try {
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        _result = qrResult;
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          _result = "The permission was denied.";
        });
      } else {
        setState(() {
          _result = "unknown error ocurred $ex";
        });
      }
    } on FormatException {
      setState(() {
        _result = "Scan canceled, try again !";
      });
    } catch (e) {
      _result = "Unknown error $e";
    }
  }
}
