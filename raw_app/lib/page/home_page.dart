import 'dart:async';

import 'package:flutter/material.dart';
import 'package:raw_app/theme/theme.dart';
import 'package:raw_app/page/wallet_page.dart';
import 'package:raw_app/page/transaction_page.dart';
import 'package:raw_app/page/me_page.dart';
import 'package:flutter_svg/flutter_svg.dart';

///主界面
class Homepage extends StatefulWidget {
  final AppTheme appTheme;
  final ValueChanged<AppTheme> onThemeChanged;

  Homepage(this.appTheme, this.onThemeChanged);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HomePageState();
  }
}

class HomePageState extends State<Homepage> {
  int _currentIndex = 0;

  SvgPicture wallet =
      new SvgPicture.asset("assets/tab_wallet_sel", color: Colors.blue);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new Scaffold(
      body: _buildNetBody(),
      bottomNavigationBar: new BottomNavigationBar(
        fixedColor: const Color(0xff2C3858),
        currentIndex: _currentIndex,
        items: [
          new BottomNavigationBarItem(
              icon: new Icon(Icons.account_balance_wallet),
              title: new Text("Wallet")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.cached), title: new Text("Transaction")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.person_outline), title: new Text("Me"))

        ],
        type: BottomNavigationBarType.fixed,
        onTap: (int selected) {
          setState(() {
            this._currentIndex = selected;
          });
        },
      ),
    );
  }

  Widget _buildNetBody() {
    final List<Widget> transitions = <Widget>[];
    transitions.add(new WalletPage());
    transitions.add(new TransactionPage());
    transitions.add(new MePage());

    return new IndexedStack(
      children: transitions,
      index: _currentIndex,
    );
  }
}
