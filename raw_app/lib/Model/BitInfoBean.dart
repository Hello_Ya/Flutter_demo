
import 'package:flutter/material.dart';


class BitInfoBean {

  var image_url ='asset/eth.png';
  var  token_name='ETH';
  var  token_value='155885.00';
  var  token_subvalue='12.01';
  var  token_money ='15353153.00';
  var  token_coins ='0.00125';
  var token_wave ="13151%";
  var  token_isup=false;

    BitInfoBean(this.image_url,this.token_name,this.token_value,this.token_subvalue,this.token_money,
        this.token_coins,this.token_wave,this.token_isup);
}
