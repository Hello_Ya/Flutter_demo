import 'package:flutter/material.dart';
import 'package:raw_app/theme/theme.dart';
import 'package:raw_app/page/home_page.dart';
void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new AppState();
  }



}

class AppState extends State<MyApp>{

       AppTheme _appTheme  =  mAllAppThemes[0];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new MaterialApp(
      title: 'REALBITWALLET',

      home: new Homepage(_appTheme,(theme){
        setState(() {
          _appTheme =theme;
        });

      }),

      theme: _appTheme.theme,
    );

  }



}